﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class BoardCreator : MonoBehaviour{


    public enum TileType
    {
        WALL,FLOOR,ROOM
    }
    public int columns = 100;
    public int rows = 100;
    public IntRange roomAmount = new IntRange(2, 3);
    public IntRange roomHeight = new IntRange(2,3);
    public IntRange roomWidth = new IntRange(2, 3);
    public IntRange hallWayLength = new IntRange(3, 5);
    public Sprite[] wallTiles;
    public Sprite[] floorTiles;
    private GameObject gameBoard;
    public TileType[] [] tileTypeArray;
    private Room[] roomArray;
    private HallWay[] hallWayArray;
    public Sprite[] hallWayTiles;
    private GameObject player;
    private GameObject currentTile;
    Material diffuse;

   

    // Use this for initialization
    private void Start () {
        
        try
        {
            gameBoard = new GameObject("Gameboard");
            setCurrentTile();
            initializeTileArray();
            createRoomsAndHalls();
            setTilesForRooms();
            setTilesForHallWays();
            gameBoardInstantiate();
            instantiatePlayer();
            Debug.Log(UnityEngine.Time.realtimeSinceStartup);
        }
        catch(IndexOutOfRangeException)
        {
            Console.WriteLine("Przechwycono wyjątek tablicy, rozpoczynam ponowną inicjalizację");
            Start();
        }
	}
	private void instantiatePlayer()
    {
        player= Resources.Load("Player/GoRi_character_transparent_6") as GameObject;
        Instantiate(player,new Vector3(rows/2,columns/2,-1.0f),Quaternion.identity);
    }

    private void setCurrentTile()
    {
        diffuse = Resources.Load("Tiles/tile_material") as Material;
        currentTile = new GameObject();
        currentTile.AddComponent<SpriteRenderer>();
        currentTile.GetComponent<SpriteRenderer>().drawMode = SpriteDrawMode.Tiled;
        currentTile.GetComponent<SpriteRenderer>().material = diffuse;
        currentTile.AddComponent<PolygonCollider2D>();
        currentTile.GetComponent<PolygonCollider2D>().autoTiling = true;
    }

    private void initializeTileArray()
    {
        tileTypeArray = new TileType[rows][];
        for(int i = 0; i < tileTypeArray.Length; i++)
        {
            tileTypeArray[i] = new TileType[columns];
        }
    }

    private void createRoomsAndHalls()
    {
        roomArray = new Room[roomAmount.getRandom()];
        hallWayArray = new HallWay[roomArray.Length - 1];

        for (int i = 0; i < roomArray.Length; i++)
        {
            if (i == 0)
            {
                roomArray[i]= new Room(roomWidth, roomHeight,rows,columns);
                hallWayArray[i]=new HallWay(roomArray[i],hallWayLength,rows,columns,roomHeight,roomWidth);
            }
            else if (i == roomArray.Length - 1)
            {
                roomArray[i] = new Room(roomWidth, roomHeight, hallWayArray[i - 1], rows, columns);
            }
            else
            {
                roomArray[i]= new Room(roomWidth, roomHeight, hallWayArray[i - 1],rows,columns);
                hallWayArray[i]=new HallWay(roomArray[i],hallWayLength,rows,columns,roomHeight,roomWidth);
            }

        }
    }

    private void setTilesForRooms()
    {
        foreach(Room r in roomArray)
        {
            for(int i=0; i <r.roomWidth; i++)
            {
                for(int j = 0; j < r.roomHeight; j++)
                {
                    tileTypeArray[r.xPos + i][r.yPos + j] = TileType.ROOM;
                }
            }
        }
    }

    private void setTilesForHallWays()
    {
            foreach (HallWay h in hallWayArray)
            {
                int x = h.startX;
                int y = h.startY;
                for (int i = 0; i <= h.hallwayLength; i++)
                {
                    switch (h.direction)
                    {
                        case HallWay.Dir.UP:
                        if ((x != 0 && x != rows - 1) && (tileTypeArray[x - 1][y + i] != TileType.WALL || tileTypeArray[x + 1][y + i] != TileType.WALL)){
                            tileTypeArray[x][y + i] = TileType.ROOM;
                        }
                        else
                        {
                            tileTypeArray[x][y + i] = TileType.FLOOR;
                        }
                            break;

                        case HallWay.Dir.DOWN:
                        if ((x != 0 && x != columns - 1) && (tileTypeArray[x - 1][y -i] != TileType.WALL || tileTypeArray[x + 1][y -i] != TileType.WALL)){
                            tileTypeArray[x][y - i] = TileType.ROOM;
                        }
                        else
                        {
                            tileTypeArray[x][y - i] = TileType.FLOOR;
                        }
                            break;

                        case HallWay.Dir.LEFT:
                        if ((y != 0 && y != rows - 1) && (tileTypeArray[x - i][y - 1] != TileType.WALL || tileTypeArray[x - i][y + 1] != TileType.WALL)){
                            tileTypeArray[x - i][y] = TileType.ROOM;
                        }
                        else
                        {
                            tileTypeArray[x - i][y] = TileType.FLOOR;
                        }
                            break;

                        case HallWay.Dir.RIGHT:
                        if ((y != 0 && y != rows - 1) && (tileTypeArray[x + i][y - 1] != TileType.WALL || tileTypeArray[x + i][y + 1] != TileType.WALL)){
                            tileTypeArray[x + i][y] = TileType.ROOM;
                        }
                        else
                        {
                            tileTypeArray[x + i][y] = TileType.FLOOR;
                        }
                        break;
                    }
                }
            }
    }
    public void randomSpriteInstantiate(Sprite [] prefabArray,int x,int y,TileType type)
    {
        Vector3 vector = new Vector3(x, y,0f);
        currentTile.GetComponent<SpriteRenderer>().sprite = prefabArray[UnityEngine.Random.Range(0, prefabArray.Length)];
        if (type.Equals(TileType.WALL))
        {
            currentTile.GetComponent<PolygonCollider2D>().enabled = true;
        }
        else
        {
            currentTile.GetComponent<PolygonCollider2D>().enabled = false;
        }
        Instantiate(currentTile, vector, Quaternion.identity).transform.parent=gameBoard.transform;
    }

    public void gameBoardInstantiate()
    {
        for(int i = 0; i < tileTypeArray.Length; i++)
        {
            for(int j = 0; j < tileTypeArray[i].Length; j++)
            {
                TileType cur = tileTypeArray[i][j];
                if (cur.Equals(TileType.ROOM))
                {
                    randomSpriteInstantiate(floorTiles, i, j,TileType.ROOM);
                }
                else if (cur.Equals(TileType.FLOOR)){
                    randomSpriteInstantiate(hallWayTiles, i, j,TileType.FLOOR);
                }
                else
                {
                    randomSpriteInstantiate(wallTiles, i, j,TileType.WALL);
                }
            }
        }
    }
}
