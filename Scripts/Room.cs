﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Room:System.Object {


    public int xPos;
    public int yPos;
    public int roomWidth;
    public int roomHeight;
    public HallWay previousHall;

    public Room(IntRange widthRange,IntRange heightRange, int rows, int columns)
    {
        roomWidth = widthRange.getRandom();
        roomHeight = heightRange.getRandom();
        xPos = Mathf.RoundToInt(rows / 2 - roomWidth / 2);
        yPos = Mathf.RoundToInt(columns / 2 - roomHeight / 2);
    }

    public Room(IntRange widthRange,IntRange heightRange,HallWay hall, int rows,int columns)
    {
        this.roomHeight = heightRange.getRandom();
        this.roomWidth = widthRange.getRandom();
        this.previousHall = hall;
        switch (hall.direction)
        {
            case HallWay.Dir.UP:
                this.roomHeight = Mathf.Clamp(roomHeight, 1, rows - hall.getEndY());          
                this.yPos = hall.getEndY();
                this.xPos = Random.Range(hall.getEndX(), hall.getEndX() - roomWidth/2);
                this.xPos = Mathf.Clamp(xPos, 0, columns - roomWidth);
                break;
            case HallWay.Dir.DOWN:
                this.roomHeight = Mathf.Clamp(roomHeight, 1, hall.getEndY());
                this.yPos = hall.getEndY()-roomHeight;
                this.xPos = Random.Range(hall.getEndX(), hall.getEndX()-roomWidth/2);
                this.xPos = Mathf.Clamp(xPos, 0, columns - roomWidth);
                break;
            case HallWay.Dir.LEFT:
                this.roomWidth = Mathf.Clamp(roomWidth, 1,columns-hall.getEndX());
                this.xPos = hall.getEndX() - roomWidth;
                this.yPos = Random.Range(hall.getEndY(), hall.getEndY()-roomHeight/2);
                this.yPos = Mathf.Clamp(yPos, 0, rows - roomHeight);
                break;
            case HallWay.Dir.RIGHT:
                this.roomWidth = Mathf.Clamp(roomWidth, 1,columns-roomWidth);
                this.xPos = hall.getEndX();
                this.yPos = Random.Range(hall.getEndY(), hall.getEndY() - roomHeight/2);
                this.yPos = Mathf.Clamp(yPos, 0, rows - roomHeight);
                break;
        }
    }
}
