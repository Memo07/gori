﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HallWay:System.Object
{
    public enum Dir
    {
        UP, RIGHT, DOWN, LEFT
    }

    public Dir direction;
    public int startX;
    public int startY;
    public int hallwayLength;

    public int getEndX()
    {
        if (direction == Dir.DOWN || direction == Dir.UP)
        {
            return startX;
        }
        else if (direction == Dir.LEFT)
        {
            return startX - hallwayLength;
        }
        return startX + hallwayLength;

    }

    public int getEndY()
    {
        if (direction == Dir.LEFT || direction == Dir.RIGHT)
        {
            return startY;
        }
        else if (direction == Dir.UP)
        {
            return startY + hallwayLength;
        }
        return startY - hallwayLength;
    }
    
   public HallWay(Room room,IntRange hlength,int rows,int columns, IntRange roomHeight,IntRange roomWidth)
    {
        int maxLength = hlength.max;
        do
        {
            this.direction = (Dir)Random.Range(0, 4);
            Dir oppositeDirection = (Dir)((int)(direction + 2) % 4);
            if (room.previousHall != null && room.previousHall.direction.Equals(oppositeDirection))
            {
                int intDirection = (int)direction;
                intDirection++;
                intDirection %= 4;
                this.direction = (Dir)intDirection;
            }
            switch (direction)
            {
                case Dir.DOWN:
                    startX = Random.Range(room.xPos, room.xPos + room.roomWidth);
                    startY = room.yPos;
                    maxLength = startY - roomHeight.max;
                    break;
                case Dir.UP:
                    startX = Random.Range(room.xPos, room.xPos + room.roomWidth);
                    startY = room.yPos + room.roomHeight;
                    maxLength = rows - roomHeight.max - startY;
                    break;
                case Dir.LEFT:
                    startX = room.xPos;
                    startY = Random.Range(room.yPos, room.yPos + room.roomHeight);
                    maxLength = startX - roomWidth.max;
                    break;
                case Dir.RIGHT:
                    startX = room.xPos + room.roomWidth;
                    startY = Random.Range(room.yPos, room.yPos + room.roomHeight);
                    maxLength = columns - roomWidth.max - startY;
                    break;
            }
            this.hallwayLength = Mathf.Clamp(hlength.getRandom(), 0, maxLength);
        } while (this.hallwayLength == 0);
    }

}