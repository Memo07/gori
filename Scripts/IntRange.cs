﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[System.Serializable]
public class IntRange: System.Object {
    public int min;
    public int max;

    public IntRange(int min,int max)
    {
        this.min = min;
        this.max = max;
    }

    public int getRandom()
    {
        return Mathf.RoundToInt(Random.Range(min, max+1));
    }
}
